<?php 
    include 'header.php'; 

    if(empty($_GET['id_jabatan'])){
        $act = 'add';
    }else {
        $act = 'edit';
        $sql = 'SELECT * FROM jabatan WHERE id_jabatan ="'.$_GET['id_jabatan'].'"';
        $query = mysqli_query($conn, $sql);
        if(mysqli_num_rows($query)){
            $act = 'edit';
            $row = mysqli_fetch_object($query);
        }
    }
?>

<h1 class="mt-3 mb-3">Form Jabatan</h1>
<form action="saveJabatan.php" method="POST">

    <input type="hidden" name="id_jabatan" value="<?php if($act =='edit') echo $row->id_jabatan?>">
    <div class="mb-3">
        <label class="form-label">Jabatan</label>
        <input type="text" class="form-control" name="jabatan"  value="<?php if($act == 'edit') echo $row->jabatan ?> " placeholder="Jabatan" required>
    </div>
    <div class="mb-3">
        <input type="submit" value="Simpan" class="btn btn-sm btn-success">
    </div>
</form>

<?php include 'footer.php'; ?>